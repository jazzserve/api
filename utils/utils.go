package utils

import (
	"crypto/rand"
	"encoding/hex"
)

func GenerateRandomString(length int) (result string) {
	data := make([]byte, length)
	rand.Read(data)
	return hex.EncodeToString(data[:])
}

// Returns hash value for combination of up to 3 int64 keys
func GenerateHashInteger(args ...int64) (hash int64) {
	magicB := int64(42)
	magicC := int64(73)

	switch len(args) {
	case 1:
		hash = mix(args[0], magicB, magicC)
	case 2:
		hash = mix(args[0], args[1], magicC)
	case 3:
		hash = mix(args[0], args[1], args[2])
	}

	return
}

// Robert Jenkins' 96 bit Mix Function
func mix(a, b, c int64) int64 {
	a = a - b
	a = a - c
	a = a ^ (c >> 13)
	b = b - c
	b = b - a
	b = b ^ (a << 8)
	c = c - a
	c = c - b
	c = c ^ (b >> 13)
	a = a - b
	a = a - c
	a = a ^ (c >> 12)
	b = b - c
	b = b - a
	b = b ^ (a << 16)
	c = c - a
	c = c - b
	c = c ^ (b >> 5)
	a = a - b
	a = a - c
	a = a ^ (c >> 3)
	b = b - c
	b = b - a
	b = b ^ (a << 10)
	c = c - a
	c = c - b
	c = c ^ (b >> 15)

	return c
}
