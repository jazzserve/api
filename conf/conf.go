package conf

import (
	"encoding/json"
	"io"
	"os"
	"path"
	"path/filepath"

	"bitbucket.org/jazzserve/api/log"
)

func InitConf(conf interface{}, confFileName string) (err error) {

	if confFileName == "" {
		confFileName = path.Base(os.Args[0]) + ".conf"
		if _, err := os.Stat(confFileName); os.IsNotExist(err) {
			confFileName = "/etc/" + confFileName
		}
	}

	confFileName, _ = filepath.Abs(confFileName)
	src, err := os.Open(confFileName)
	if err != nil {
		return
	}
	err = loadConf(src, conf)
	if err != nil {
		return
	}

	log.Info.Printf("-conf=%s", confFileName)
	return
}

func loadConf(src io.Reader, conf interface{}) (err error) {
	decoder := json.NewDecoder(src)
	err = decoder.Decode(&conf)
	if err != nil {
		return
	}
	return
}
