package db

import (
	"database/sql"
	"errors"
	"fmt"
	"io/ioutil"
	"reflect"
	"strconv"
	"strings"

	_ "github.com/lib/pq"

	"bitbucket.org/jazzserve/api/log"
)

type NativeDb struct {
	*sql.DB
}

var (
	ErrInvalidRow = errors.New("db: invalid row")
	ErrNoRows     = sql.ErrNoRows
	ErrNoRights   = errors.New("sql: you have no rights to do this operation")
)

func NewNativeDb(conf *DBConfiguration) (dbNative *NativeDb, err error) {
	db, err := sql.Open(conf.Driver, conf.getConnectionString())
	if err != nil {
		return
	}

	setConnectionPool(db, conf)
	dbNative = &NativeDb{DB: db}
	return
}

func (Db *NativeDb) GetDBVersion() (version int, err error) {
	err = Db.QueryRow("SELECT version FROM db_version").Scan(&version)
	return
}

func (Db *NativeDb) setDbVersion(dbVersion int) (err error) {
	return Db.ExecSQL("UPDATE db_version SET version = $1", dbVersion)
}

func (Db *NativeDb) DbUpgrade(initPath string, from int, to int) (err error) {
	if from > to {
		return errors.New(fmt.Sprintf("Database not upgrade. Version %d > %d", from, to))
	}
	if from == to {
		log.Info.Println(fmt.Sprintf("Database already upgraded. Version %d", to))
		return
	}
	var sql []byte
	for dbVersion := from + 1; dbVersion <= to; dbVersion++ {
		fileName := initPath + "/" + strconv.Itoa(dbVersion) + ".sql"
		log.Info.Println(fileName)
		sql, err = ioutil.ReadFile(fileName)
		if err != nil {
			err = errors.New(fmt.Sprintf("file %s not found", fileName))
			break
		}
		err = Db.ExecSQL(string(sql))
		if err != nil {
			break
		}
		err = Db.setDbVersion(dbVersion)
		if err != nil {
			break
		}
	}
	return
}

func (Db *NativeDb) GetJson(sql string, args ...interface{}) (result []byte, notfound bool, err error) {
	log.TraceDb.Println(sql)
	log.TraceDb.Printf("%#v", args)
	rows, err := Db.Query(fmt.Sprintf("SELECT to_json(t) FROM (%s) t", sql), args...)
	if err != nil {
		return
	}
	defer rows.Close()
	if !rows.Next() {
		notfound = true
		return
	}
	err = rows.Scan(&result)
	return
}

func (Db *NativeDb) GetJsonAgg(sql string, args ...interface{}) (result []byte, err error) {
	log.TraceDb.Println(sql)
	log.TraceDb.Printf("%#v", args)
	err =
		Db.QueryRow(fmt.Sprintf("SELECT COALESCE(json_agg(t), '[]') FROM (%s) t", sql), args...).
			Scan(&result)
	return
}

func (Db *NativeDb) GetJsonAggTx(tx *sql.Tx, sql string, args ...interface{}) (result []byte, err error) {
	log.TraceDb.Println(sql)
	log.TraceDb.Printf("%#v", args)
	err =
		tx.QueryRow(fmt.Sprintf("SELECT COALESCE(json_agg(t), '[]') FROM (%s) t", sql), args...).
			Scan(&result)
	return
}

func (Db *NativeDb) ExecSQL(sql string, args ...interface{}) (err error) {
	log.TraceDb.Println(sql)
	log.TraceDb.Printf("%#v", args)

	stmt, err := Db.Prepare(sql)
	if err != nil {
		return
	}
	_, err = stmt.Exec(args...)

	return
}

func ExecSQLTx(tx *sql.Tx, sql string, args ...interface{}) (err error) {
	log.TraceDb.Println(sql)
	log.TraceDb.Printf("%#v", args)

	stmt, err := tx.Prepare(sql)
	if err != nil {
		return
	}
	_, err = stmt.Exec(args...)

	return
}

func GetInsertQuery(tableName string, fields map[string]interface{}) (sqlQuery string, args []interface{}, err error) {
	sqlQuery = "INSERT INTO %s (%s) SELECT %s RETURNING id" // 1. Table Name; 2. Fields; 3. Values;

	fieldNameParams := make([]string, 0)
	fieldValuesParams := make([]string, 0)

	for k, v := range fields {
		fieldNameParams = append(fieldNameParams, k)
		args = append(args, v)
		fieldValuesParams = append(fieldValuesParams, "$"+strconv.Itoa(len(args)))
	}

	fieldNames := strings.Join(fieldNameParams, ", ")
	fieldValues := strings.Join(fieldValuesParams, ", ")

	sqlQuery = fmt.Sprintf(sqlQuery, tableName, fieldNames, fieldValues)

	return
}

func GetUpdateQuery(tableName string, fields map[string]interface{}) (sqlQuery string, args []interface{}, err error) {
	sqlQuery = "UPDATE %s SET %s WHERE id = $1 RETURNING id" // 1. Table Name; 2. Values; 3. id;

	args = append(args, fields["id"]) // get id
	delete(fields, "id")

	fieldValuesParams := make([]string, 0)

	for k, v := range fields {
		fieldValuesParams = append(fieldValuesParams, k+" = $"+(strconv.Itoa(len(args)+1)))
		args = append(args, v)
	}

	fieldValues := strings.Join(fieldValuesParams, ", ")
	sqlQuery = fmt.Sprintf(sqlQuery, tableName, fieldValues)

	return
}

func MapFieldsToValues(field string, dataSet map[string]interface{}, v interface{}) {
	if reflect.TypeOf(v).Kind() == reflect.Ptr && reflect.ValueOf(v).Pointer() != 0 {
		dataSet[field] = &v
	}
}

func MapObjectIdToValues(field string, dataSet map[string]interface{}, o interface{}) {
	obj := reflect.ValueOf(o)
	var f reflect.Value
	if obj.Kind() == reflect.Ptr && reflect.ValueOf(o).Pointer() != 0 {
		f = obj.Elem().FieldByName("Id")

	} else if obj.Kind() == reflect.Struct {
		f = obj.FieldByName("Id")
	}

	if f.IsValid() {
		if f.Kind() == reflect.Int64 && f.Int() == 0 {
			dataSet[field] = nil
		} else {
			dataSet[field] = f.Interface()
		}
	}
}
