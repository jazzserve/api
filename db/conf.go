package db

import (
	"database/sql"
	"fmt"

	"bitbucket.org/jazzserve/api/log"
)

//DBConfiguration example
//"DB" :  {
//	  "ApplicationName" : "alpen-api",
//	  "Database" : "TEST_LOC",
//	  "Driver" : "postgres",
//	  "Host" : "postgres.jazzserve.com",
//	  "MaxIdleConns" : 5,
//	  "MaxOpenConns" : 5,
//	  "Password" : "password",
//	  "Port" : 5432,
//	  "User" : "user",
//	 },
type DBConfiguration struct {
	Driver          string
	User            string
	Password        string
	Host            string
	Port            int
	Database        string
	MaxIdleConns    int
	MaxOpenConns    int
	ApplicationName string
	SSLMode         string
}

func (conf *DBConfiguration) getConnectionString() string {
	if conf == nil {
		log.Error.Panicln("Db conf is empty")
	}

	return fmt.Sprintf("%s://%s:%s@%s:%v/%s?application_name=%s&sslmode=%s", conf.Driver,
		conf.User, conf.Password, conf.Host, conf.Port, conf.Database,
		conf.ApplicationName, conf.SSLMode)
}

func setConnectionPool(db *sql.DB, conf *DBConfiguration) {
	if conf.MaxIdleConns != 0 {
		db.SetMaxIdleConns(conf.MaxIdleConns)
	}
	if conf.MaxOpenConns != 0 {
		db.SetMaxOpenConns(conf.MaxOpenConns)
	}
}
