package db

import (
	"bitbucket.org/jazzserve/gorm"
	_ "bitbucket.org/jazzserve/gorm/dialects/postgres"
)

func NewOrmDb(conf *DBConfiguration) (db *gorm.DB, err error) {
	db, err = gorm.Open(conf.Driver, conf.getConnectionString())
	if err != nil {
		return
	}

	setConnectionPool(db.DB(), conf)
	db.SingularTable(true)
	return
}
