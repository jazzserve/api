package db

import (
	"fmt"

	"bitbucket.org/jazzserve/gorm"
	_ "bitbucket.org/jazzserve/gorm/dialects/postgres"
)

type LocalizedDb struct {
	*gorm.DB
}

var LocalizationList map[string]bool

func (d *LocalizedDb) Locale(l string) *gorm.DB {
	if LocalizationList[l] {
		return d.Set("locale", l)
	}
	return d.Set("locale", "")
}

func (d *LocalizedDb) AutoMigrate(values ...interface{}) *gorm.DB {

	/*db := d.Unscoped()
	for _, value := range values {
		//db = db.NewScope(value).autoMigrate().db
	}*/

	/*d.DB = d.DB.AutoMigrate(values...)
	d.autoLocaleMigrate(values...)

	return d.DB
	*/

	for _, value := range values {
		d.DB = d.DB.AutoMigrate(value)
		d.DB = d.autoMigrateLocale(value)

	}
	return d.DB

}

func (d *LocalizedDb) autoMigrateLocale(value interface{}) *gorm.DB {
	s := d.DB.NewScope(value)

	tableName := s.TableName()
	quotedTableName := s.QuotedTableName()

	if s.Dialect().HasTable(tableName) {
		for _, field := range s.GetModelStruct().StructFields {
			locName := field.Tag.Get("locale")
			if locName != "" && s.Dialect().HasColumn(tableName, field.DBName) {
				for locale := range LocalizationList {
					if locale != "" && field.IsNormal &&
						!s.Dialect().HasColumn(tableName, locName+"_"+locale) {
						sqlTag := s.Dialect().DataTypeOf(field)
						s.Raw(fmt.Sprintf("ALTER TABLE %v ADD %v %v;", quotedTableName, s.Quote(locName+"_"+locale), sqlTag)).Exec()
					}
				}
			}
		}
	}

	return s.DB()
}

func changeLocalizedField(scope *gorm.Scope) {
	l := ""
	if v, ok := scope.Get("locale"); ok {
		l = v.(string)
		if l != "" {
			l = "_" + l
		}
	}

	items := scope.GetStructFields()
	for i := range items {
		f := items[i]

		if locName := f.Tag.Get("locale"); locName != "" {
			f.DBName = locName + l
		}

	}
}

func initLocalizationList(localizationValues ...string) {
	LocalizationList = make(map[string]bool)
	LocalizationList[""] = true

	for i := range localizationValues {
		LocalizationList[localizationValues[i]] = true
	}

}

func NewLocalizedDb(conf *DBConfiguration, localizationValues ...string) (dbLoc *LocalizedDb, err error) {
	initLocalizationList(localizationValues...)
	db, err := gorm.Open(conf.Driver, conf.getConnectionString())
	if err != nil {
		return
	}
	setConnectionPool(db.DB(), conf)
	db.SingularTable(true)
	db.Callback().Query().Before("gorm:query").Register("localization:before_query", changeLocalizedField)
	dbLoc = &LocalizedDb{DB: db}
	return
}
