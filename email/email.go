package email

import (
	"crypto/tls"
	"fmt"
	"net/smtp"
	"strconv"
	"strings"
	"errors"

	"bitbucket.org/jazzserve/api/log"
	"bitbucket.org/jazzserve/api/utils"
	"encoding/base64"
)

type EmailConfiguration struct {
	UserName   string `valid:"required" toml:"user-name"`
	Password   string `valid:"required" toml:"password"`
	Host       string `valid:"required,url" toml:"host"`
	Port       int    `valid:"required" toml:"port"`
	From       string `valid:"required,email" toml:"from"`
	ReplyTo    string `valid:"email" toml:"reply-to"`
	SenderName string `toml:"sender-name"`
	SSL        bool   `toml:"ssl"`
}

var (
	Email *EmailStruct
)

type EmailStruct struct {
	conf *EmailConfiguration
}

type EmailBodyPart struct {
	ContentType             string
	ContentTransferEncoding string
	Body                    string
}

func NewEmail(conf *EmailConfiguration) *EmailStruct {
	return &EmailStruct{conf: conf}
}

func (email *EmailStruct) sendMail(to []string, msg []byte) error {
	auth := LoginAuth(email.conf.UserName, email.conf.Password)
	if email.conf.SSL {
		return sslEmail(email.conf.Host, email.conf.Port, auth, email.conf.From, to, msg)
	}
	return smtp.SendMail(email.conf.Host+":"+strconv.Itoa(email.conf.Port), auth, email.conf.From, to, msg)
	//
}

func (email *EmailStruct) SendMultipartEmail(to []string, subject string, emailBodyParts []EmailBodyPart) (err error) {
	log.Info.Println(fmt.Sprintf("Send email: %s", to))

	msgId := utils.GenerateRandomString(32)

	header := make(map[string]string)
	header["MIME-Version"] = "1.0"
	header["From"] = email.conf.SenderName + "<" + email.conf.From + ">"
	header["To"] = strings.Join(to, ",")
	header["Reply-to"] = email.conf.From
	header["Subject"] = subject
	header["Content-Type"] = "multipart/alternative;\r\n  boundary=" + `"` + msgId + `"`
	//header["  boundary="] = `"` + msgId + `"`

	message := ""
	for k, v := range header {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}

	message += "\r\n"

	for _, part := range emailBodyParts {
		message += "--" + msgId
		message += "\r\n"
		if part.ContentType != "" {
			message += "Content-Type: " + part.ContentType
			message += "\r\n"
		}
		if part.ContentTransferEncoding != "" {
			message += "Content-Transfer-Encoding: " + part.ContentTransferEncoding
			message += "\r\n"
		}
		message += "\r\n"
		message += part.Body
		message += "\r\n"
	}

	message += "--" + msgId + "--"

	err = email.sendMail(to, []byte(message))

	if err != nil {
		log.Error.Println(fmt.Sprintf("Send email: %s. Error: %v", to, err.Error()))
	}

	return

}

func (email *EmailStruct) SendTextMessage(to string, subject string, message string) error {
	return email.SendMultipartEmail([]string{to}, subject, []EmailBodyPart{email.GetTextPart(message)})
}

func (email *EmailStruct) SendHTMLMessage(to string, subject string, message string) error {
	return email.SendMultipartEmail([]string{to}, subject, []EmailBodyPart{email.GetHTMLPart(message)})
}

func (email *EmailStruct) GetTextPart(body string) (part EmailBodyPart) {
	part.ContentType = "text/plain; charset=UTF-8"
	part.ContentTransferEncoding = "base64"
	part.Body = base64.StdEncoding.EncodeToString([]byte(body))
	return part
}

func (email *EmailStruct) GetHTMLPart(body string) (part EmailBodyPart) {
	part.ContentType = "text/html; charset=UTF-8"
	part.ContentTransferEncoding = "base64"
	part.Body = base64.StdEncoding.EncodeToString([]byte(body))
	return part
}

type loginAuth struct {
	username, password string
}

func LoginAuth(username, password string) smtp.Auth {
	return &loginAuth{username, password}
}

func (a *loginAuth) Start(server *smtp.ServerInfo) (string, []byte, error) {
	return "LOGIN", []byte{}, nil
}

func (a *loginAuth) Next(fromServer []byte, more bool) ([]byte, error) {
	if more {
		switch string(fromServer) {
		case "Username:":
			return []byte(a.username), nil
		case "Password:":
			return []byte(a.password), nil
		default:
			return nil, errors.New("Unknown fromServer")
		}
	}
	return nil, nil
}

func sslEmail(host string, port int, a smtp.Auth, from string, to []string, msg []byte) error {
	addr := host + ":" + strconv.Itoa(port)

	c, err := smtp.Dial(addr)
	if err != nil {
		return err
	}

	tlsconfig := &tls.Config{
		ServerName: host,
	}
	if err = c.StartTLS(tlsconfig); err != nil {
		return err
	}

	if err = c.Auth(a); err != nil {
		return err
	}

	if err = c.Mail(from); err != nil {
		return err
	}

	for _, addrTo := range to {
		if err = c.Rcpt(addrTo); err != nil {
			return err
		}
	}

	w, err := c.Data()
	if err != nil {
		return err
	}

	_, err = w.Write(msg)
	if err != nil {
		return err
	}

	err = w.Close()
	if err != nil {
		return err
	}

	c.Quit()
	return err
}
