package validation

import (
	"testing"
)

func TestNewValidator(t *testing.T) {
	if err := NewValidator().Validate(); err != nil {
		t.Errorf("error while create empty validator: %s", err.Error())
	}
}

func TestValidator_AddRule_simpleField(t *testing.T) {
	errString := ""
	normalString := "i'm string"

	v1 := NewValidator().AddRule("errString", errString, NotNull)
	if err := v1.Validate(); err == nil {
		t.Errorf("expected err but it's not found")
	}

	v2 := NewValidator().AddRule("normalString", normalString, NotNull)
	if err := v2.Validate(); err != nil {
		t.Errorf("unexpected err: %s", err.Error())
	}
}

func TestValidator_AddRule_pointerField(t *testing.T) {
	var errString *string
	normalString := new(string)

	v1 := NewValidator().AddRule("errPointerString", errString, NotNull)
	if err := v1.Validate(); err == nil {
		t.Errorf("expected err but it's not found")
	}

	v2 := NewValidator().AddRule("normalPointerString", normalString, NotNull)
	if err := v2.Validate(); err != nil {
		t.Errorf("unexpected err: %s", err.Error())
	}
}

func TestValidator_AddRule_simpleStruct(t *testing.T) {
	type Activity struct {
		Id *string
	}

	type Strategy struct {
		Id       *int
		Activity Activity
	}

	errStrategy := Strategy{ }
	normalStrategy := Strategy{ Activity: Activity{ Id: new(string) }}

	v1 := NewValidator().AddRule("errStrategy.Activity", errStrategy.Activity, EntityIsIncluded)
	if err := v1.Validate(); err == nil {
		t.Errorf("expected err but it's not found")
	}

	v2 := NewValidator().AddRule("normalStrategy.Activity", normalStrategy.Activity, EntityIsIncluded)
	if err := v2.Validate(); err != nil {
		t.Errorf("unexpected err: %s", err.Error())
	}
}

func TestValidator_AddRule_pointerStruct(t *testing.T) {
	type Activity struct {
		Id *string
	}

	type Strategy struct {
		Id       *int
		Activity *Activity
	}

	errStrategy := Strategy{ }
	errStrategy2 := Strategy{ Activity: &Activity{} }
	normalStrategy := Strategy{ Activity: &Activity{ Id: new(string) }}

	v1 := NewValidator().AddRule("errStrategy.Activity", errStrategy.Activity, EntityIsIncluded)
	if err := v1.Validate(); err == nil {
		t.Errorf("expected err but it's not found")
	}

	v2 := NewValidator().AddRule("errStrategy2.Activity", errStrategy2.Activity, EntityIsIncluded)
	if err := v2.Validate(); err == nil {
		t.Errorf("expected err but it's not found")
	}

	v3 := NewValidator().AddRule("normalStrategy.Activity", normalStrategy.Activity, EntityIsIncluded)
	if err := v3.Validate(); err != nil {
		t.Errorf("unexpected err: %s", err.Error())
	}
}

func TestValidator_AddRule_slice(t *testing.T) {
	type Activity struct {
		Id *string
	}

	normalActivityList := []*Activity{}

	v1 := NewValidator().AddRule("errStrategy.Activity", normalActivityList, NotNull, EntityIsIncluded)
	if err := v1.Validate(); err != nil {
		t.Errorf("expected err but it's not found")
	}
}