package validation

import (
	"errors"
	"fmt"
	"log"
	"reflect"
)

/**
*	This package is useful for validation of incoming structures from WEB clients
 */

type rule int

const (
	NotNull          = rule(0)
	IsNull           = rule(1)
	EntityIsIncluded = rule(2)
)

func (r rule) String() string {
	var ruleName string

	switch r {
	case NotNull:
		ruleName = "Not null"
	case IsNull:
		ruleName = "Is null"
	case EntityIsIncluded:
		ruleName = "Entity is included"
	}

	return fmt.Sprintf("rule [name=%s]", ruleName)
}

type checkPoint struct {
	value     interface{}
	fieldName string
	ruleList  []rule
}

type validator struct {
	checkPointList []*checkPoint
}

func NewValidator() *validator {
	return &validator{}
}

func (v *validator) AddRule(fieldName string, value interface{}, rules ...rule) *validator {
	vu := &checkPoint{
		fieldName: fieldName,
		value:     value,
		ruleList:  rules,
	}

	v.checkPointList = append(v.checkPointList, vu)

	return v
}

func (v *validator) Validate() (fullError error) {
	var errorList []error
	errorMessage := "validation errors: \n"

	for _, unit := range v.checkPointList {
		err := unit.performValidation()
		if err != nil {
			errorList = append(errorList, err...)
		}
	}

	if len(errorList) != 0 {
		for _, err := range errorList {
			errorMessage += fmt.Sprintf("\t* %s\n", err.Error())
		}

		fullError = errors.New(errorMessage)
	}

	return fullError
}

func (u *checkPoint) performValidation() (errList []error) {
	t := reflect.TypeOf(u.value)

	switch t.Kind() {
	case reflect.Bool,
		reflect.Int,
		reflect.Int32,
		reflect.Int64,
		reflect.Float32,
		reflect.Float64,
		reflect.String:
		errList = u.validateAsField()
	case reflect.Struct:
		errList = u.validateAsEntity()
	case reflect.Slice:
		errList = u.validateAsSlice()
	case reflect.Ptr:
		switch t.Elem().Kind() {
		case reflect.Bool,
			reflect.Int,
			reflect.Int32,
			reflect.Int64,
			reflect.Float32,
			reflect.Float64,
			reflect.String:
			errList = u.validateAsFieldPointer()
		case reflect.Struct:
			errList = u.validateAsEntityPointer()
		}
	}

	return
}

func (u *checkPoint) validateAsField() (errList []error) {
	t := reflect.TypeOf(u.value)

	for _, rule := range u.ruleList {
		switch rule {
		case NotNull:
			msg := "'" + u.fieldName + "' has default value"
			switch t.Kind() {
			case reflect.Int:
				if u.value.(int) == int(0) {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Int32:
				if u.value.(int32) == int32(0) {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Int64:
				if u.value.(int64) == int64(0) {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Float32:
				if u.value.(float32) == float32(0) {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Float64:
				if u.value.(float64) == float64(0) {
					errList = append(errList, errors.New(msg))
				}
			case reflect.String:
				if u.value.(string) == "" {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Bool:
				if u.value.(bool) == false {
					errList = append(errList, errors.New(msg))
				}
			default:
				printUnsupportedValidationMessage(u.fieldName, rule)
			}
		case IsNull:
			msg := "'" + u.fieldName + "' is not empty"
			switch t.Kind() {
			case reflect.Int:
				if u.value.(int) != int(0) {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Int32:
				if u.value.(int32) != int32(0) {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Int64:
				if u.value.(int64) != int64(0) {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Float32:
				if u.value.(float32) != float32(0) {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Float64:
				if u.value.(float64) != float64(0) {
					errList = append(errList, errors.New(msg))
				}
			case reflect.String:
				if u.value.(string) != "" {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Bool:
				if u.value.(bool) != false {
					errList = append(errList, errors.New(msg))
				}
			default:
				printUnsupportedValidationMessage(u.fieldName, rule)
			}
		case EntityIsIncluded:
			printUnsupportedValidationMessage(u.fieldName, rule)
		}
	}

	return
}

func (u *checkPoint) validateAsFieldPointer() (errList []error) {
	t := reflect.TypeOf(u.value)

	for _, rule := range u.ruleList {
		switch rule {
		case NotNull:
			msg := "'" + u.fieldName + "' is null"
			switch t.Elem().Kind() {
			case reflect.Int:
				if u.value.(*int) == nil {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Int32:
				if u.value.(*int32) == nil {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Int64:
				if u.value.(*int64) == nil {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Float32:
				if u.value.(*float32) == nil {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Float64:
				if u.value.(*float64) == nil {
					errList = append(errList, errors.New(msg))
				}
			case reflect.String:
				if u.value.(*string) == nil {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Bool:
				if u.value.(*bool) == nil {
					errList = append(errList, errors.New(msg))
				}
			default:
				printUnsupportedValidationMessage(u.fieldName, rule)
			}
		case IsNull:
			msg := "'" + u.fieldName + "' is not null"
			switch t.Elem().Kind() {
			case reflect.Int:
				if u.value.(*int) != nil {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Int32:
				if u.value.(*int32) != nil {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Int64:
				if u.value.(*int64) != nil {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Float32:
				if u.value.(*float32) != nil {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Float64:
				if u.value.(*float64) != nil {
					errList = append(errList, errors.New(msg))
				}
			case reflect.String:
				if u.value.(*string) != nil {
					errList = append(errList, errors.New(msg))
				}
			case reflect.Bool:
				if u.value.(*bool) != nil {
					errList = append(errList, errors.New(msg))
				}
			default:
				printUnsupportedValidationMessage(u.fieldName, rule)
			}
		case EntityIsIncluded:
			printUnsupportedValidationMessage(u.fieldName, rule)
		}
	}

	return
}

func (u *checkPoint) validateAsEntity() (errList []error) {
	t := reflect.ValueOf(u.value)

	for _, rule := range u.ruleList {
		switch rule {
		case NotNull:
			printUnsupportedValidationMessage(u.fieldName, rule)
		case IsNull:
			printUnsupportedValidationMessage(u.fieldName, rule)
		case EntityIsIncluded:
			field := t.FieldByName("Id")
			if !field.IsValid() {
				errList = append(errList, errors.New("'"+u.fieldName+"' has no 'Id' field"))
				break
			}

			switch field.Type().Kind() {
			case reflect.Ptr:
				if field.IsNil() {
					errList = append(errList, errors.New("'"+u.fieldName+"' has empty 'Id' field"))
				}
			default:
				// If not a pointer, then any value is OK
			}
		}
	}

	return
}

func (u *checkPoint) validateAsEntityPointer() (errList []error) {
	t := reflect.ValueOf(u.value).Elem()
	for _, rule := range u.ruleList {
		switch rule {
		case NotNull:
			if u.value == nil {
				errList = append(errList, errors.New("'"+u.fieldName+"' is null"))
			}
		case IsNull:
			if u.value == nil {
				errList = append(errList, errors.New("'"+u.fieldName+"' is not null"))
			}
		case EntityIsIncluded:
			if reflect.ValueOf(u.value).IsNil() {
				errList = append(errList, errors.New("'"+u.fieldName+"' is null"))
				break
			}

			field := t.FieldByName("Id")
			if !field.IsValid() {
				errList = append(errList, errors.New("'"+u.fieldName+"' has no 'Id' field"))
				break
			}

			switch field.Type().Kind() {
			case reflect.Ptr:
				if field.IsNil() {
					errList = append(errList, errors.New("'"+u.fieldName+"' has empty 'Id'"))
				}
			default:
				// If id isn't pointer it's should have default value
			}
		}
	}

	return
}

func (u *checkPoint) validateAsSlice() (errList []error) {
	for _, rule := range u.ruleList {
		switch rule {
		case NotNull:
			printUnsupportedValidationMessage(u.fieldName, NotNull)
		case EntityIsIncluded:
			printUnsupportedValidationMessage(u.fieldName, EntityIsIncluded)
		}
	}

	return
}

func printUnsupportedValidationMessage(fieldName string, rule rule) {
	msg := "WARNING: validation %s wan't be applied to '%s', it's unsupported"
	log.Println(fmt.Sprintf(msg, rule, fieldName))
}
