package sessionstore

import "time"

const (
	network            = "tcp"
	defaultCookiePath  = "/"
	defaultMaxIdle     = 1
	defaultIdleTimeOut = 5
)

type Config struct {
	Address string         `yaml:"address" valid:"required"`
	Key     string         `yaml:"key" valid:"required"`
	MaxAge  *time.Duration `yaml:"max-age" valid:"required"`
	DB      int            `yaml:"db" valid:"required"`
}
