package helper

import (
	"net/url"
	"time"
)

//	Safe pointer value getters

func BoolValue(p *bool) (v bool) {
	if p != nil {
		v = *p
	}

	return
}

func IntValue(p *int) (v int) {
	if p != nil {
		v = *p
	}

	return
}

func Int32Value(p *int32) (v int32) {
	if p != nil {
		v = *p
	}

	return
}

func Int64Value(p *int64) (v int64) {
	if p != nil {
		v = *p
	}

	return
}

func Float32Value(p *float32) (v float32) {
	if p != nil {
		v = *p
	}

	return
}

func Float64Value(p *float64) (v float64) {
	if p != nil {
		v = *p
	}

	return
}

func StringValue(p *string) (v string) {
	if p != nil {
		v = *p
	}

	return
}

func TimeValue(p *time.Time) (v time.Time) {
	if p != nil {
		v = *p
	}

	return
}

//	Pointers evaluators

func BoolPointer(v bool) *bool {
	return &v
}

func IntPointer(v int) *int {
	return &v
}

func Int32Pointer(v int32) *int32 {
	return &v
}

func Int64Pointer(v int64) *int64 {
	return &v
}

func Float32Pointer(v float32) *float32 {
	return &v
}

func Float64Pointer(v float64) *float64 {
	return &v
}

func StringPointer(v string) *string {
	return &v
}

func TimePointer(v time.Time) *time.Time {
	return &v
}

//	Constructors

func NewEmptyParams() *url.Values {
	return &url.Values{}
}
