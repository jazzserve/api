package webapi

import (
	"fmt"
	"net/http"

	"bitbucket.org/jazzserve/api/log"
)

var MapRoute = MapControllers{}

func InitMapRoute() {
	for name, controller := range MapRoute {
		http.HandleFunc(fmt.Sprintf("/%s/%s", API, name), controller.ServeHTTP)
		controller.AllowActions = "OPTIONS, "
		for action, _ := range controller.Actions {
			controller.AllowActions = controller.AllowActions + action + ","
		}
		controller.Name = name
		log.Info.Printf("%s %s", name, controller.AllowActions)
	}
}
