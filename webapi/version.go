package webapi

import "time"

type VersionResponse struct {
	DBVersion    int
	AppName      string
	AppVersion   string
	AppBuildDate time.Time
}

var Version = &VersionResponse{}
