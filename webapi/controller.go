package webapi

import (
	"encoding/json"
	"net/http"
	"reflect"

	"bitbucket.org/jazzserve/api/log"
	"encoding/xml"
	"errors"
)

type MapControllers map[string]*Controller

type MapActions map[string]*Action

type ActionFunc func(context *Context) (data interface{}, errcode int, err error)

type Action struct {
	Auth        bool
	Func        ActionFunc
	RequestType reflect.Type
	Roles       []string
}

type Controller struct {
	Actions      MapActions
	Name         string
	AllowActions string
}

func internal(context Context) (data interface{}, errcode int, err error) {
	return context.Action.Func(&context)
}

func setOrigin(c *Controller, w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Methods", c.AllowActions)
	w.Header().Set("Access-Control-Allow-Origin", WebConf.StaticUrl)
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("P3P", WebConf.P3PHeader)
	for _, o := range WebConf.Origins {
		if o == r.Header.Get("Origin") {
			w.Header().Set("Access-Control-Allow-Origin", o)
		}
	}
}

func (c *Controller) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Trace.Println(r.Method, r.URL)

	setOrigin(c, w, r)
	w.Header().Set(ContentType, ApplicationJson)
	if r.Method == OPTIONS {
		w.Header().Set("Access-Control-Allow-Headers", "origin, content-type, accept")
		return
	}

	var err error
	var ok bool
	action, ok := c.Actions[r.Method]
	if !ok {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed),
			http.StatusMethodNotAllowed)
		return
	}

	var context = Context{
		Request:    r,
		Response:   w,
		Controller: c,
		Action:     action,
	}

	if action.Auth {
		session, err := context.GetSession()
		if err != nil {
			log.Error.Print(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if session == nil {
			http.Error(w, http.StatusText(http.StatusUnauthorized),
				http.StatusUnauthorized)
			return
		} else {
			context.Session = session
		}
	}

	if action.RequestType != nil {
		requestData := reflect.New(context.Action.RequestType)
		decoder := json.NewDecoder(context.Request.Body)
		err = decoder.Decode(requestData.Interface())
		if err != nil {
			log.Error.Print(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		context.RequestData = requestData.Interface()
	}

	if r.Method == GET || r.Method == DELETE {
		err = context.Request.ParseForm()
		if err != nil {
			log.Error.Print(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	responseData, errcode, err := internal(context)
	if errcode != 0 {
		if err != nil {
			http.Error(w, err.Error(), errcode)
		} else {
			http.Error(w, http.StatusText(errcode), errcode)
		}
		return
	}
	if err != nil {
		log.Error.Print(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	//	Write response if it exists
	if responseData != nil {
		switch context.Response.Header().Get(ContentType) {
		case ApplicationJson:
			responseJson, err := json.Marshal(responseData)
			if err != nil {
				log.Error.Print(err)
				err = errors.New("error marshaling response (json)")

				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			_, err = context.Response.Write(responseJson)
		case ApplicationXml:
			responseXml, err := xml.Marshal(responseData)
			if err != nil {
				log.Error.Print(err)
				err = errors.New("error marshaling response (xml)")

				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			_, err = context.Response.Write(responseXml)
		default:
			//	If we've got bytes - write though
			if responseBytes, ok := responseData.([]byte); ok {
				_, err = context.Response.Write(responseBytes)
			}
		}

		if err != nil {
			return
		}
	}

	return
}
