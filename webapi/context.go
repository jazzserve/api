package webapi

import (
	"net/http"

	"bitbucket.org/jazzserve/api/session"
)

type Context struct {
	Response    http.ResponseWriter
	Request     *http.Request
	Session     *session.Session
	Controller  *Controller
	Action      *Action
	RequestData interface{}
}

func (context *Context) SetSession(sessionData session.SessionData) (err error) {
	return session.CreateSession(context.Request, context.Response, sessionData, SESSION_COOKIE_NAME)
}

func (context *Context) SaveSession() (err error) {
	return context.Session.SaveSession(context.Request, context.Response)
}

func (context *Context) GetSession() (s *session.Session, err error) {
	return session.GetSession(context.Request, context.Response, SESSION_COOKIE_NAME)
}

func (context *Context) RemoveSession() (err error) {
	return session.RemoveSession(context.Request, context.Response, context.Session)
}
