package webapi

const (
	API                 = "api"
	WS                  = "ws"
	SESSION_COOKIE_NAME = "SessionId"

	ApplicationJson = "application/json; charset=utf-8"
	ApplicationXml  = "application/xml; charset=utf-8"

	GET     = "GET"
	POST    = "POST"
	OPTIONS = "OPTIONS"
	PUT     = "PUT"
	DELETE  = "DELETE"
)

// Default values
const (
	DefaultMaxMemory = 256 * 1024 // 256MB

	DefaultLimit  = 50
	DefaultOffset = 0
)

//	Headers
const (
	ContentType        = "Content-Type"
	ContentDisposition = "Content-Disposition"
)

//	MIME types
const (
	ApplicationMSOfficePresentation = "application/vnd.openxmlformats-officedocument.presentationml.presentation"
)
