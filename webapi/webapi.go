package webapi

//WebConfigurationExample
//"Web":{
//      "Port":4040,
//      "StaticUrl":"http://localhost:62040",
//      "P3PHeader":"CP=\"CAO PSA OUR\""
//   }
type WebConfiguration struct {
	Port         int
	StaticUrl    string
	P3PHeader    string
	CookieDomain string
	Origins      []string
}

var (
	WebConf *WebConfiguration
)

func NewWebApi(conf *WebConfiguration, version *VersionResponse) {
	WebConf = conf
	Version = version
	InitMapRoute()

	return
}
