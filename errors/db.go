package errors

import (
	"errors"
	"fmt"
)

func NewCreateEntityException(entity fmt.Stringer, message string) error {
	return newDBException("creating", entity, message)
}

func NewReadingEntityException(entity fmt.Stringer, message string) error {
	return newDBException("reading", entity, message)
}

func NewUpdateEntityException(entity fmt.Stringer, message string) error {
	return newDBException("updating", entity, message)
}

func NewDeleteEntityException(entity fmt.Stringer, message string) error {
	return newDBException("deleting", entity, message)
}

func NewMigrationException(entity fmt.Stringer, message string) error {
	return newDBException("migration", entity, message)
}

func NewSeedingException(entity fmt.Stringer, message string) error {
	return newDBException("seeding", entity, message)
}

func newDBException(action string, entity fmt.Stringer, reason string) error {
	return errors.New(fmt.Sprintf("error %s '%s': \n\t%s", action, entity.String(), reason))
}

func New(message string) error {
	return errors.New(message)
}
