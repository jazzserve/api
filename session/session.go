//Nead InitSessionStore before use!!!
package session

import (
	"bitbucket.org/jazzserve/api/sessionstore"
	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
	"net/http"
)

var sessionDataType interface{}

type Session struct {
	Data           SessionData
	gorillaSession *sessions.Session
}

type SessionData interface {
	GetSessionKey() string
}

var store *sessionstore.SessionStore

func InitSessionStore(config *sessionstore.Config, dataType SessionData) (err error) {
	store, err = sessionstore.New(config)
	if err != nil {
		return
	}

	sessionDataType = dataType
	return nil
}

func (session *Session) SaveSession(r *http.Request, w http.ResponseWriter) (err error) {
	err = store.Interpreter.PutObj(session.gorillaSession, session.Data)
	if err != nil {
		return err
	}

	if err = session.gorillaSession.Save(r, w); err != nil {
		return err
	}

	return nil
}

func CreateSession(r *http.Request, w http.ResponseWriter, sessionData SessionData, name string) error {
	r, err := store.Interpreter.PutObjType(r, sessionDataType)
	if err != nil {
		return err
	}

	r, err = sessionstore.PutUserID(r, sessionData.GetSessionKey())
	if err != nil {
		return err
	}

	gorillaSession, err := store.Get(r, name)
	if err != nil {
		return err
	}

	err = store.Interpreter.PutObj(gorillaSession, sessionData)
	if err != nil {
		return err
	}

	gorillaSession.ID = ""
	if err = gorillaSession.Save(r, w); err != nil {
		return err
	}

	return nil
}

func GetSession(r *http.Request, w http.ResponseWriter, name string) (*Session, error) {
	r, err := store.Interpreter.PutObjType(r, sessionDataType)
	if err != nil {
		return nil, err
	}

	gorillaSession, err := store.Get(r, name)
	if err != nil {
		return nil, err
	}

	obj := store.Interpreter.GetObj(gorillaSession)
	if obj == nil {
		return nil, nil
	}

	sessionData, ok := obj.(SessionData)
	if !ok {
		return nil, errors.New("Fatal error: can't convert session obj to SessionData")
	}

	session := &Session{
		Data:           sessionData,
		gorillaSession: gorillaSession,
	}

	return session, nil
}

func RemoveSession(r *http.Request, w http.ResponseWriter, session *Session) error {
	session.gorillaSession.Options.MaxAge = -1
	return session.gorillaSession.Save(r, w)
}

func UpdateAllByPattern(pattern string, value interface{}) error {
	return store.AppendAllSessionsDirectly(pattern, value)
}

func UpdateAllByPatternDynamic(pattern string, getter sessionstore.ObjGetter) error {
	return store.AppendAllSessionsDirectlyByF(pattern, getter)
}
