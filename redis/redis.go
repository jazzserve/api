//How to use example:
//import (
//	"bitbucket.org/jazzserve/api/redis"
//)
//func main() {
//	redis, err := redis.NewRedisStorage(RedisConfiguration)
//	if err != nil {
//		return
//	}
//}

package redis

import (
	"strconv"
	"time"

	"bitbucket.org/jazzserve/api/log"
	"github.com/garyburd/redigo/redis"
)

//RedisConfiguration example
/*
"Redis":{
      "Host":"127.0.0.1",
      "Port":6379,
      "SessionDb":9
   },
*/
type RedisConfiguration struct {
	Host      string
	Port      int
	SessionDb int
}

type RedisStorage struct {
	*redis.Pool
}

func NewRedisStorage(conf *RedisConfiguration) (redisPool *RedisStorage, err error) {
	if conf == nil {
		log.Error.Panicln("Redis configuration is empty")
	}
	redisPool = &RedisStorage{
		Pool: RedisNewPool(conf.Host+":"+strconv.Itoa(conf.Port), conf.SessionDb),
	}

	if !redisPool.Ping() {
		log.Error.Println("NewRedisStorage: can't connect to redis server")
	}
	return
}

func RedisNewPool(server string, dbNum int) *redis.Pool {
	log.Info.Printf("Redis: %s, Db: %v", server, dbNum)
	return &redis.Pool{
		MaxIdle:     1,
		IdleTimeout: 5,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server, redis.DialDatabase(dbNum))
			if err != nil {
				return nil, err
			}
			return c, err
		},
		/*TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			if err != nil {
				log.Info.Println("Redis error")
			}
			return err
		},*/
	}
}

func (pool *RedisStorage) Ping() bool {
	conn := pool.Get()
	data, err := redis.String(conn.Do("PING"))
	defer conn.Close()
	if err != nil || data != "PONG" {
		return false
	}
	return true
}

func (redisPool *RedisStorage) GetKey(key string) (data string, err error) {
	conn := redisPool.Get()
	data, err = redis.String(conn.Do("GET", key))
	defer conn.Close()
	if err != nil {
		return
	}
	return
}

func (redisPool *RedisStorage) GetKeys(keyMask string) (data []string, err error) {
	conn := redisPool.Get()
	data, err = redis.Strings(conn.Do("KEYS", keyMask))
	defer conn.Close()
	if err != nil {
		return
	}
	return
}

func (redisPool *RedisStorage) SetKey(key string, data string) (err error) {
	conn := redisPool.Get()
	_, err = conn.Do("SET", key, data)
	defer conn.Close()
	if err != nil {
		return
	}

	return
}

func (redisPool *RedisStorage) GetExpire(key string) (exp time.Time, err error) {
	conn := redisPool.Get()
	data, err := redis.Int64(conn.Do("TTL", key))
	defer conn.Close()
	if err != nil {
		return
	}
	exp = time.Now().Add(time.Second * time.Duration(data))
	return
}

func (redisPool *RedisStorage) SetExpire(key string, exp time.Time) (err error) {
	conn := redisPool.Get()
	_, err = conn.Do("EXPIREAT", key, exp.Unix())
	defer conn.Close()
	if err != nil {
		return
	}
	return
}

func (redisPool *RedisStorage) DeleteKey(key string) (err error) {
	conn := redisPool.Get()
	_, err = conn.Do("DEL", key)
	defer conn.Close()
	if err != nil {
		return
	}
	return
}

func (redisPool *RedisStorage) DeleteKeys(keyMask string) (err error) {
	keys, err := redisPool.GetKeys(keyMask)
	if err != nil {
		return
	}
	conn := redisPool.Get()
	defer conn.Close()
	for _, v := range keys {
		_, err = conn.Do("DEL", v)
		if err != nil {
			return
		}
	}
	return
}
