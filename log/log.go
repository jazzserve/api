package log

import (
	"io"
	"io/ioutil"
	"log"
	"os"
)

var (
	Trace   *log.Logger
	TraceDb *log.Logger
	Info    *log.Logger
	Warning *log.Logger
	Error   *log.Logger
)

func InitLog(trace bool, traceDb bool) {
	var traceHandle io.Writer
	var traceDbHandle io.Writer
	if trace {
		traceHandle = os.Stdout
	} else {
		traceHandle = ioutil.Discard
	}
	if traceDb {
		traceDbHandle = os.Stdout
	} else {
		traceDbHandle = ioutil.Discard
	}
	CreateLogs(traceHandle, traceDbHandle, os.Stdout, os.Stdout, os.Stderr)
}

func CreateLogs(
	traceHandle io.Writer,
	traceDbHandle io.Writer,
	infoHandle io.Writer,
	warningHandle io.Writer,
	errorHandle io.Writer) {
	Trace = log.New(traceHandle, "TRACE: ", 0)
	TraceDb = log.New(traceDbHandle, "TRACEDB: ", 0)
	Info = log.New(infoHandle, "", 0)
	Warning = log.New(warningHandle, "WARNING: ", 0)
	Error = log.New(errorHandle, "ERROR: ", log.Lshortfile)
}
