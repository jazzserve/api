package command

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

type Command struct {
	Function func() (err error)
	Params   string
	Doc      string
	Commands CommandsMap
}

type CommandsMap map[string]*Command

func HelpCommands(commands CommandsMap, commandStr string) {
	fmt.Printf("Usage: %s %s %s [options]\n\n", filepath.Base(os.Args[0]), commandStr, "COMMAND")
	fmt.Println("List of Commands:")
	for c, command := range commands {
		fmt.Printf("  %s %s - %s\n", c, command.Params, command.Doc)
	}
}

func RunCommand(commands CommandsMap, commandStr string, level int) (err error) {
	var commandInt string
	var command *Command
	if len(os.Args) > level+1 {
		commandInt = os.Args[level+1]
		command = commands[commandInt]
	}
	if command == nil {
		fmt.Println("You need to give some command")
		HelpCommands(commands, commandStr)
	} else {
		fmt.Println(command.Doc)
		if command.Commands != nil {
			err = RunCommand(command.Commands, commandInt, level+1)
		} else {
			argsCount := 0
			if command.Params != "" {
				argsCount = len(strings.Split(command.Params, " "))
			}
			if len(os.Args) < argsCount+level+2 {
				fmt.Printf("Parameters: %s\n", command.Params)
				return
			}
			err = command.Function()
		}
	}
	return
}
